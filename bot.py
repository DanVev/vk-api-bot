import os
import vk_api
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
from vk_api.utils import get_random_id
import log
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from database import Database
from collections import defaultdict
import strings
from random import choice

START_GAME = False
IS_REGISTRATION_OPEN = True
MONEY_PER_KILL = 10
TEAM_MONEY_PER_KILL = 2


class Bot:
    def __init__(self):
        self.db = Database()
        log.info("Establishing VK session ...")
        self.vk_session = vk_api.VkApi(token=os.environ['vk_token'])
        self.vk = self.vk_session.get_api()
        self.upload = vk_api.VkUpload(self.vk_session)
        self.longpoll = VkBotLongPoll(self.vk_session, 185876717)
        self.user_status = {}
        self.handlers = defaultdict(list)
        log.info("Session is started")

    def start(self):
        log.info("Start listening ...")
        if START_GAME and not self.db.is_game_started:
            self.start_game()
#        else:
#            for user_id in self.db.users:
#                self.send_start_menu(user_id)
        while True:
            try:
                for event in self.longpoll.listen():
                    if event.type == VkBotEventType.MESSAGE_NEW:
                        user_id = event.obj.from_id
                        is_handlered = self.process_handlers(event, user_id)
                        log.info(f"New message from: {user_id} Text: {event.obj.text}")
                        if not is_handlered:
                            if event.obj.text == "Зарегистрироваться" \
                                    and not self.is_user_registered(user_id) \
                                    and IS_REGISTRATION_OPEN:
                                self.register_user(user_id)
                            elif event.obj.text == "Статус игрока":
                                self.print_status(user_id)
                            elif self.db.is_game_started and self.is_user_active(
                                    user_id) and event.obj.text == "Захват":
                                self.ask_target_code(user_id)
                            elif self.db.is_game_started and self.is_user_active(
                                    user_id) and event.obj.text == "Моя цель":
                                self.show_target(user_id)
                            else:
                                self.send_start_menu(user_id)
            except Exception as e:
                log.exception(e)

    def start_game(self):
        log.info("Starting a game ...")
        self.db.create_teams()
        for user in self.db.users.values():
            target_user = self.db.users[user.target]
            photo_object_dict = self.upload.photo_messages(target_user.photo)[0]
            self.vk.messages.send(
                peer_id=user.user_id,
                random_id=get_random_id(),
                message=f"{strings.GAME_START}\n"
                f"Твой персональный код: {user.personal_code}\n"
                f"Номер гильдии: {user.team_number}\n"
                f"Твоя первая цель:",
                attachment=f"photo{photo_object_dict['owner_id']}_{photo_object_dict['id']}",
                keyboard=self.get_start_keyboard(user.user_id)
            )
        self.db.is_game_started = True
        self.db.dump_db()
        log.info("Game is started")

    def register_handler(self, function, user_id):
        log.debug(f"{user_id}: Registering handler {function.__name__}...")
        self.handlers[user_id].append(function)

    def process_handlers(self, event, user_id):
        if self.handlers[user_id]:
            handler = self.handlers[user_id].pop()
            log.debug(f"{user_id}: Executing handler {handler.__name__}...")
            handler(event)
            return True
        return False

    def show_target(self, user_id):
        user = self.db.users[user_id]
        if not user.target:
            log.warning(f"User {user_id} has no target")
        else:
            if user.target == user.user_id:
                self.vk.messages.send(
                    peer_id=user_id,
                    random_id=get_random_id(),
                    message=strings.TARGET_IF_WINNER,
                    keyboard=self.get_start_keyboard(user_id)
                )
            else:
                self.send_user_name_and_photo(user_id, user.target)

    def ask_target_code(self, user_id):
        self.vk.messages.send(
            peer_id=user_id,
            random_id=get_random_id(),
            message=strings.PERSONAL_CODE_ASK
        )
        self.register_handler(self.check_target_code, user_id)

    def check_target_code(self, event):
        user_id = event.obj.from_id
        user = self.db.users[user_id]
        target = self.db.users[user.target]
        if self.is_user_active(user_id):
            if target.personal_code == event.obj.text.strip():
                log.info(f"User {user_id} is killing {target.user_id} ...")
                self.kill(user_id)
            else:
                self.vk.messages.send(
                    peer_id=user_id,
                    random_id=get_random_id(),
                    message=strings.PERSONAL_CODE_WRONG,
                    keyboard=self.get_start_keyboard(user_id)
                )
        else:
            self.send_start_menu(user_id)

    def kill(self, user_id):
        user = self.db.users[user_id]
        target = self.db.users[user.target]
        user.num_of_kills += 1
        self.db.teams[user.team_number]['money'] += TEAM_MONEY_PER_KILL
        target.is_active = False
        self.vk.messages.send(
            peer_id=target.user_id,
            random_id=get_random_id(),
            message=strings.LOSER_F.format(user.name),
            keyboard=self.get_start_keyboard(target.user_id)
        )
        self.handlers[target.user_id].clear()
        user.target = target.target
        if user.target == user_id:
            self.vk.messages.send(
                peer_id=user_id,
                random_id=get_random_id(),
                message=strings.WINNER,
                keyboard=self.get_start_keyboard(user_id)
            )
        else:
            self.vk.messages.send(
                peer_id=user_id,
                random_id=get_random_id(),
                message=choice(strings.TARGET_KILL_L),
            )
            self.send_user_name_and_photo(user_id, user.target)
        self.db.dump_db()

    def send_user_name_and_photo(self, user_id, target_user_id):
        target = self.db.users[target_user_id]
        photo_object_dict = self.upload.photo_messages(target.photo)[0]
        self.vk.messages.send(
            peer_id=user_id,
            random_id=get_random_id(),
            message=strings.TARGET_NEXT,
            attachment=f"photo{photo_object_dict['owner_id']}_{photo_object_dict['id']}",
            keyboard=self.get_start_keyboard(user_id)
        )

    def is_user_registered(self, user_id):
        return user_id in self.db.users and self.db.users[user_id].is_registered

    def is_user_active(self, user_id):
        return user_id in self.db.users and self.db.users[user_id].is_active

    def print_status(self, user_id):
        text = None
        if user_id in self.db.users:
            user = self.db.users[user_id]
            text = f"Имя игрока: {user.name}\nСтатус регистрации: {'Пройдена' if user.is_registered else 'Не пройдена'}"
            if user.is_registered:
                if self.db.is_game_started:
                    text = f"Имя игрока: {user.name}\nНомер гильдии: {user.team_number}\n" \
                        f"Персональный код: {user.personal_code}\n\n" \
                        f"Статус регистрации: {'Пройдена' if user.is_registered else 'Не пройдена'}\n" \
                        f"Статус игрока: {'В игре' if user.is_active else 'Захвачен'}\n\n" \
                        f"Кол-во золота: {user.num_of_kills * MONEY_PER_KILL}\U0001f4b0\n" \
                        f"Кол-во золота в гильдии: {self.db.teams[user.team_number]['money']} \U0001f4b0\n\n" \
                        f"Загруженная фотография: "
                photo_object_dict = self.upload.photo_messages(user.photo)[0]
                self.vk.messages.send(
                    peer_id=user_id,
                    random_id=get_random_id(),
                    message=text,
                    attachment=f"photo{photo_object_dict['owner_id']}_{photo_object_dict['id']}",
                    keyboard=self.get_start_keyboard(user_id)
                )
            else:
                self.vk.messages.send(
                    peer_id=user_id,
                    random_id=get_random_id(),
                    message=text,
                    keyboard=self.get_start_keyboard(user_id)
                )

        else:
            text = "Статус регистрации: Не пройдена"
            self.vk.messages.send(
                peer_id=user_id,
                random_id=get_random_id(),
                message=text,
                keyboard=self.get_start_keyboard(user_id)
            )

    def send_start_menu(self, user_id):
        self.vk.messages.send(
            peer_id=user_id,
            random_id=get_random_id(),
            keyboard=self.get_start_keyboard(user_id),
            message=strings.START_MENU
        )

    def get_start_keyboard(self, user_id):
        keyboard = VkKeyboard(one_time=True)
        keyboard.add_button('Статус игрока', color=VkKeyboardColor.PRIMARY)

        if not self.is_user_registered(user_id):
            keyboard.add_line()
            # Переход на вторую строку
            keyboard.add_button('Зарегистрироваться', color=VkKeyboardColor.POSITIVE)
        else:
            if self.is_user_active(user_id) and self.db.is_game_started:
                if self.db.users[user_id].target != user_id:
                    keyboard.add_line()
                    keyboard.add_button('Захват', color=VkKeyboardColor.POSITIVE)

                keyboard.add_line()
                keyboard.add_button('Моя цель', color=VkKeyboardColor.NEGATIVE)
        return keyboard.get_keyboard()

    def register_user(self, user_id):
        user_name = self.vk.users.get(user_id=user_id)[0]
        self.confirm_choice(user_id, f"Начнем с твоего имени и фамилии. Тебя зовут: "
        f"{user_name['first_name']} {user_name['last_name']}?\n"
        f"Важно, чтобы это были твои реальные имя и фамилия")
        self.register_handler(self.check_name, user_id)

    def ask_photo(self, user_id):
        self.vk.messages.send(
            peer_id=user_id,
            random_id=get_random_id(),
            message="Загрузи свою фотографию.\n"
                    "1. На фотографии должно быть только твоё лицо\n"
                    "2. Лицо должно быть сфотографировано в анфас\n"
                    "3. Части лица не должны закрывать волосы, грим, маска и/или солнцезащитные очки"
        )
        self.register_handler(self.get_photo, user_id)

    def ask_name(self, user_id):
        self.vk.messages.send(
            peer_id=user_id,
            random_id=get_random_id(),
            message=strings.NAME_ASK
        )
        self.register_handler(self.get_name, user_id)

    def check_name(self, event):
        user_id = event.obj.from_id
        if event.obj.text == 'Да':
            user_name = self.vk.users.get(user_id=user_id)[0]
            self.db.add_user(user_id, f"{user_name['first_name']} {user_name['last_name']}")
            self.ask_photo(user_id)
        else:
            self.ask_name(user_id)

    def get_name(self, event):
        user_id = event.obj.from_id
        name = event.obj.text
        self.db.add_user(user_id, name)
        self.ask_photo(user_id)

    def get_photo(self, event):
        user_id = event.obj.from_id
        for attachment in event.obj.attachments:
            if attachment['type'] == 'photo':
                photo = attachment['photo']
                photo_url = None
                for size in photo['sizes']:
                    if size['type'] == 'x':
                        photo_url = size['url']
                        break
                self.db.save_photo(photo_url, user_id)
                self.vk.messages.send(
                    peer_id=user_id,
                    random_id=get_random_id(),
                    message=strings.REGISTRATION_END,
                    keyboard=self.get_start_keyboard(user_id)
                )
                self.db.dump_db()
                return

    def confirm_choice(self, user_id, message):
        keyboard = VkKeyboard(one_time=True)
        keyboard.add_button('Да', color=VkKeyboardColor.POSITIVE)
        keyboard.add_button('Нет', color=VkKeyboardColor.NEGATIVE)

        self.vk.messages.send(
            peer_id=user_id,
            keyboard=keyboard.get_keyboard(),
            random_id=get_random_id(),
            message=message
        )


if __name__ == '__main__':
    Bot().start()
