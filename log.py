import os
import logging
import sys


class StdWriter:
    def __init__(self, logger, level, stream):
        self.logger = logger
        self.level = level
        self.std_stream = stream

    def write(self, message):
        if message.strip():
            self.logger.log(self.level, message.strip())
        # self.std_stream.write(message)

    def flush(self):
        # self.std_stream.flush()
        flush()


if not os.path.exists("logs"):
    os.mkdir("logs")

logger = logging.getLogger("log")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('[{asctime}] [{levelname}]: {message}', style='{', datefmt='%Y-%m-%d %H:%M:%S')

fh_info = logging.FileHandler(os.path.join("logs", "info.log"), mode='w', encoding='UTF-8')
fh_info.setFormatter(formatter)
fh_info.setLevel(logging.INFO)

fh_debug = logging.FileHandler(os.path.join("logs", "debug.log"), mode='w', encoding='UTF-8')
fh_debug.setFormatter(formatter)
fh_debug.setLevel(logging.DEBUG)

fh_error = logging.FileHandler(os.path.join("logs", "error.log"), mode='w', encoding='UTF-8')
fh_error.setFormatter(formatter)
fh_error.setLevel(logging.ERROR)

ch = logging.StreamHandler()
ch.setFormatter(formatter)
ch.setLevel(logging.DEBUG)

logger.addHandler(fh_info)
logger.addHandler(fh_debug)
logger.addHandler(fh_error)

logger.addHandler(ch)


def info(*message):
    logger.info(" ".join(str(x) for x in message))


def error(*message):
    logger.error(" ".join(str(x) for x in message))


def debug(*message):
    logger.debug(" ".join(str(x) for x in message))


def warning(*message):
    logger.warning(" ".join(str(x) for x in message))


def critical(*message):
    logger.critical(" ".join(str(x) for x in message))


def exception(exception):
    logger.exception(exception)


def write(message):
    error(message)


def flush():
    logger.setLevel(logging.DEBUG)


sys.stdout = StdWriter(logger, logging.INFO, sys.stdout)
sys.stderr = StdWriter(logger, logging.ERROR, sys.stderr)
