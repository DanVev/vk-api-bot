import os
from os import path as osp
import json
import urllib
import shutil
import log
import string
import random

NUM_OF_TEAMS = 4


class Database:
    def __init__(self):
        self.folder = osp.join(os.getcwd(), 'database')
        self.users = {}
        self.photo_folder = osp.join(os.getcwd(), 'photos')
        self.is_game_started = False
        self.teams = {}
        self.load_db()

    def load_db(self):
        log.info("Loading a database ...")
        if not osp.exists(self.folder):
            return
        try:
            with open(osp.join(self.folder, 'db_info.json'), 'r') as f:
                data = json.load(f)
                self.is_game_started = data['is_game_started']
                self.teams = {int(key):data['teams'][key] for key in data['teams']}
        except Exception as e:
            log.warning(f"Failed to load database infodata")
            log.exception(e)
        for user_id in os.listdir(self.folder):
            if osp.isdir(osp.join(self.folder, user_id)):
                try:
                    with open(osp.join(self.folder, user_id, 'info.json'), 'r', encoding='utf-8') as f:
                        data = json.load(f)
                        log.debug(f"User loaded: {data}")
                        self.users[int(user_id)] = User(**data)
                    photo_path = osp.join(self.folder, user_id, 'photo.jpg')
                    if osp.exists(photo_path):
                        shutil.copyfile(photo_path, osp.join(self.photo_folder, f'{user_id}.jpg'))
                except Exception as e:
                    log.warning(f"Failed to load data for user {user_id}")
                    log.exception(e)

    def dump_db(self):
        log.info("Dumping a database ...")
        if not osp.exists(self.folder):
            os.mkdir(self.folder)
        data = {'is_game_started': self.is_game_started, 'teams': self.teams}
        with open(osp.join(self.folder, 'db_info.json'), 'w') as f:
            json.dump(data, f, indent=4)
        for user_id in self.users:
            user_folder = osp.join(self.folder, str(user_id))
            if not osp.exists(user_folder):
                os.mkdir(user_folder)
            with open(osp.join(user_folder, 'info.json'), 'w', encoding='utf-8') as f:
                json.dump(self.users[user_id].dump(), f, indent=4, ensure_ascii=False)
            photo_path = osp.join(self.photo_folder, f"{user_id}.jpg")
            if osp.exists(photo_path):
                shutil.copyfile(photo_path, osp.join(user_folder, 'photo.jpg'))

    def save_photo(self, url, user_id):
        if not osp.exists(self.photo_folder):
            os.mkdir(self.photo_folder)
        if url:
            photo_path = osp.join(self.photo_folder, f"{user_id}.jpg")
            urllib.request.urlretrieve(url, photo_path)
            self.users[user_id].photo = photo_path
            self.users[user_id].is_registered = True
            log.info(f"User {user_id} is registered")

    def add_user(self, user_id, name):
        self.users[user_id] = User(name, user_id)

    def create_teams(self):
        log.info("Creating teams ...")
        players = list(self.users.values())
        random.shuffle(players)
        teams = [list() for _ in range(NUM_OF_TEAMS)]
        for index, player in enumerate(players):
            team_number = (index % NUM_OF_TEAMS) + 1
            player.team_number = team_number
            teams[team_number-1].append(player)
        for i in range(max([len(x) for x in teams])):
            circle = []
            for team_index in range(len(teams)):
                if i < len(teams[team_index]):
                    circle.append(teams[team_index][i])
            if len(circle) <= 1:
                log.warning(f"Circle is almost empty, players: {[x.user_id for x in circle]}")
            for index in range(len(circle)):
                circle[index].target = circle[index - 1].user_id

        for i, team in enumerate(teams):
            team_ids = [user.user_id for user in team]
            log.debug(f"Team {i+1} is registered. Player ids: {team_ids}")
            self.teams[i+1] = {'players': team_ids, 'money': 0}

        with open('teams.txt', 'w', encoding='utf-8') as f:
            for team_index, team in enumerate(teams):
                print(f"########## Team #{(team_index + 1):02} ##########", file=f)
                for player_index, player in enumerate(team):
                    print(f"{(player_index + 1):02}  {player.user_id:9}  {player.target:9}  {player.personal_code:7}  {player.name}", file=f)
        log.info("File teams.txt is created")


class User:
    def __init__(self, name, user_id, **kwargs):
        self.personal_code = kwargs.get('personal_code', self.generate_personal_code())
        self.team_number = kwargs.get('team_number', None)
        self.target = kwargs.get('target', None)
        self.name = name
        self.user_id = user_id
        self.is_registered = kwargs.get('is_registered', False)
        self.photo = kwargs.get('photo', None)
        self.num_of_kills = kwargs.get('num_of_kills', 0)
        self.is_active = kwargs.get('is_active', True)

    def dump(self):
        return self.__dict__

    def __repr__(self):
        return f"User {self.user_id} Name: {self.name}"

    @staticmethod
    def generate_personal_code():
        return "".join(random.choice(string.ascii_lowercase) for _ in range(7))


class Game:
    def __init__(self):
        pass
